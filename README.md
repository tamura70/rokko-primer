# Rokko-primer

Rokko-primer is a theme of [Hugo](https://gohugo.io) static site generator.

See the demo site at [Netlify](https://www.netlify.com).

- <https://rokko-primer.netlify.com>

## Example of config.toml

```
# Hugo configuration
baseURL = ""
theme = "rokko-primer"
pygmentsCodeFences = true
pygmentsUseClasses = true
# disablePathToLower = true
# paginate = 20
DefaultContentLanguage = "ja"
ignoreFiles = "/hidden/"
rssLimit = 100

[outputs]
home = ["HTML", "RSS"]
section = ["HTML"]
page = ["HTML"]

[blackfriday]
# smartypants = false

[languages.ja]
languageCode = "ja"
title = "Rokko-primerの利用ガイド"
footer = "&copy;2019-- Tao Kay Uranium"
hasCJKLanguage = true
# summaryLength = 70
# pluralizeListTitles = false

[languages.en]
languageCode = "en"
title = "Rokko-primer User's Guide"
footer = "&copy;2019-- Tao Kay Uranium"
summaryLength = 70

[[languages.ja.menu.main]]
identifier = "tags"
name = "タグ"
url = "/tags/"
weight = 1

[[languages.ja.menu.main]]
identifier = "categories"
name = "カテゴリー"
url = "/categories/"
weight = 2

[[languages.en.menu.main]]
name = "Tags"
url = "/en/tags/"
weight = 1

[[languages.en.menu.main]]
name = "Categories"
url = "/en/categories/"
weight = 2

[[languages.ja.menu.links]]
name = "Hugo"
url = "https://gohugo.io"
weight = 1

[params]
netlify_cms = true
description = "Rokko-primer User's Guide"
lastmodFormat = "2006-01-02"
enable_list = true
list_class = ""
# list_class = "card"
# mathjax = true
# debug_show_variables = true

[params.navigations]
alert_pages = 1
toc = 1
section_toc = 1
related = 2
links = 2
breadcrumb = true
tags = true
categories = true

[params.section_toc]
section_mark = "&sect; "
max = 100

[params.related]
max = 100

[related]
threshold = 80
includeNewer = false
toLower = false

[[related.indices]]
name = "keywords"
weight = 100

[[related.indices]]
name = "categories"
weight = 80

[[related.indices]]
name  = "tags"
weight = 20

[[related.indices]]
name  = "date"
weight = 10

# [params.list]
# header = "<h2>List of Pages</h2>"
# section = "."
# pages = [ "sections", "pages" ]
# tags = []
# categories = []
# sortby = "title"

[params.list_style]
section_mark = "&sect; "
lastmodFormat = "(2006-01-02)"
pages_count = true
tags = true
categories = true
summary = true

```
